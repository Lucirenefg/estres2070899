/*
	Name: Examen Final
	Copyright: TecNM
	Authores:
	Fuentes Gonzales Lucirene 
	Gratz Viveros Ana Karen
	Ramos Gonzalez Arlette
	Valdez L�pez Miriam
	Date: 25/05/19 18:26
	Description: "CALCULADORA"
*/
// ********************************* INICIA SECCION DE DECLARATIVAS *************************
#include <iostream>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include <cstring>

using namespace std;
// ********************************* TERMINA SECCION DE DECLARATIVAS *************************

// ********************************* INICIA SECCION DE DECLARACIONES *************************
int a,b;// DECLARACION DE LOS NUMEROS A INGRESAR 
float suma,resta,multiplicacion,division,potencia,raiz;//DECLARACION DE VARIABLES 
char eleccion;//DECLARACION DE ELECCION 
char repeatop;//DECLARACION PARA LA OPERACION 
int arreglo[5]= {1,2,3,4,5};//DECLARACION DEL ARREGLO
int resultado= 0 ;//DECLARACION PARA LOS RESULTADOS
// ********************************* TERMINA SECCION DE DECLARACIONES *************************
